class CfgPatches
{
	class dzr_vehicle_parts_degradation
	{
		requiredAddons[] = {"DZ_Data"};
		units[] = {};
		weapons[] = {};
	};
};

class CfgMods
{
	class dzr_vehicle_parts_degradation
	{
		type = "mod";
		author = "DayZRussia.com";
		description = "Parts wear out when used";
		dir = "dzr_vehicle_parts_degradation";
		name = "DZR Vehicle Parts Degradation";
		//inputs = "dzr_vehicle_parts_degradation/Data/Inputs.xml";
		dependencies[] = {"Core", "Game", "World", "Mission"};
		class defs
		{
			class engineScriptModule
			{
				files[] = {"dzr_vehicle_parts_degradation/Common",  "dzr_vehicle_parts_degradation/1_Core"};
			};
			class gameLibScriptModule
			{
				files[] = {"dzr_vehicle_parts_degradation/Common",  "dzr_vehicle_parts_degradation/2_GameLib"};
			};
			class gameScriptModule
			{
				files[] = {"dzr_vehicle_parts_degradation/Common",  "dzr_vehicle_parts_degradation/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_vehicle_parts_degradation/Common", "dzr_vehicle_parts_degradation/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_vehicle_parts_degradation/Common", "dzr_vehicle_parts_degradation/5_Mission"};
			};

		};
	};
};
class CfgVehicles
{
	class Inventory_Base;
	class DZR_VPD_Explosion: Inventory_Base
	{
		class NoiseTireExplosion
		{
			strength = 180;
			type = "shot";
		};
	};
};
class CfgSoundShaders
{
	class DZR_VPD_ExplosionSoundShader
	{
		samples[] = {{"\dzr_vehicle_parts_degradation\sound\TireExplosion",1.0}};
		volume = 0.7;
		range = 250;
		frequencyRandomizer=1;
		volumeRandomizer=1;
		frequencyFactor=1.1;
	};
};
class CfgSoundSets
{
	class DZR_VPD_ExplosionSoundSet
	{
		sound3DProcessingType = "infected3DProcessingType";
		volumeCurve = "infectedShoutingAttenuationCurve";
		spatial = 1;
		doppler = 0;
		loop = 0;
		soundShaders[] = {"DZR_VPD_ExplosionSoundShader"};
	};
};